# Aliases for interacting with IBM Q Experience
# =============================================


# Login
alias ibmq-login='curl -s -d "apiToken=$IBMQ_API_TOKEN" "https://api.quantum-computing.ibm.com/api/users/loginWithToken" > ~/.ibmq_credentials'


# List of backends
alias ibmq-backends='curl -s "`jq -r \"@uri \\\"https://api.quantum-computing.ibm.com/api/Backends?access_token=\(.id)\\\"\" ~/.ibmq_credentials`"'


# Get calibration information for a given processor
alias ibmq-backend-calibration='sh -c '\''curl -s "`jq -r \"@uri \\\"https://api.quantum-computing.ibm.com/api/Backends/$@/calibration?access_token=\(.id)\\\"\" ~/.ibmq_credentials`"'\'' _'


# Get backend parameters for a given processor
alias ibmq-backend-parameters='sh -c '\''curl -s "`jq -r \"@uri \\\"https://api.quantum-computing.ibm.com/api/Backends/$@/parameters?access_token=\(.id)\\\"\" ~/.ibmq_credentials`"'\'' _'


# Get the status of a processor's queue
alias ibmq-backend-queue='sh -c '\''curl -s "https://api.quantum-computing.ibm.com/api/Backends/$@/queue/status"'\'' _'


# List jobs in the execution queue
alias ibmq-queue='curl -s "`jq -r \"@uri \\\"https://api.quantum-computing.ibm.com/api/Jobs?access_token=\(.id)\\\"\" ~/.ibmq_credentials`"'


# Get the API version
alias ibmq-api-version='curl -s "`jq -r \"@uri \\\"https://api.quantum-computing.ibm.com/api/version?access_token=\(.id)\\\"\" ~/.ibmq_credentials`"'


# Info about user
alias ibmq-user='curl -s "`jq -r \"@uri \\\"https://api.quantum-computing.ibm.com/api/users/\(.userId)?access_token=\(.id)\\\"\" ~/.ibmq_credentials`"'


# Latest experiments
alias ibmq-experiments='curl -s "`jq -r \"@uri \\\"https://api.quantum-computing.ibm.com/api/users/\(.userId)/codes/latest?access_token=\(.id)&includeExecutions=true\\\"\" ~/.ibmq_credentials`"'


# Run experiment
alias ibmq-submit='curl -s -H "Content-Type: application/json" --data @- "`jq -r \"@uri \\\"https://api.quantum-computing.ibm.com/api/Jobs?access_token=\(.id)\\\"\" ~/.ibmq_credentials`"'


# Info about a job
alias ibmq-job-info='sh -c '\''curl -s "`jq -r \"@uri \\\"https://api.quantum-computing.ibm.com/api/Jobs/$@?access_token=\(.id)\\\"\" ~/.ibmq_credentials`"'\'' _'

