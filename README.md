# ibmq-shell

Set of shell aliases for interacting with IBM Q Experience API. Single requirements are `curl` and `jq`.

_The aliases should work with most shells._

## Install

Add the following two lines in your shell configuration file:

    export IBMQ_API_TOKEN="XXXXXXXX_MY_API_TOKEN_XXXXXXXX"
    . /path/to/ibmq-shell/ibmq-shell.sh

## Usage

You must login to your account before any other operation:

    ibmq-login

which will write to `~/.ibmq_credentials`; whenever the credentials are expired, just run the previous command again.

### Basic commands

The following aliases return some informations as JSON objects:

    ibmq-user        # information about the use account
    ibmq-backends    # list of all available backends
    ibmq-experiments # list of latest experiments
    ibmq-queue       # list of jobs in the execution queue
    ibmq-api-version # get the API version

Since output of all commands is JSON, it can also be filtered with `jq`:

    ibmq-backends | jq ".[] | .name"
    ibmq-backends | jq -c ".[] | { name: .name, nQubits: .nQubits }"
    ibmq-queue | jq length
    ibmq-queue | jq -c '.[] | { id: .id, date: .creationDate, status: .status }'

### Submitting a new job

A new job can be submitted (as a QOBJ embedded in JSON format) as:

    ibmq-submit < qobj_test.json
    cat qobj_test.json | ibmq-submit

_(See QOBJ example files in the current repository.)_

The ID of the submitted job can be fetched with

    ibmq-submit < qobj_test.json | jq ".id"

Information about the submitted job is retrieved with

    ibmq-job-info ID_OF_THE_JOB
    ibmq-job-info ID_OF_THE_JOB | jq ".status"

# More specific commands

Calibration parameters for a given processor are retrieved with

    ibmq-backend-calibration NAME_OF_THE_BACKEND

Backend parameters for a given processor are retrieved with

    ibmq-backend-parameters NAME_OF_THE_BACKEND

Status of a processor's queue is retrieved with

    ibmq-backend-queue NAME_OF_THE_BACKEND
